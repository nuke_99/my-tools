#!/usr/bin/env ruby
require 'readline'

# Author : Nuke99
# Twitter : https://twitter.com/nuke_99
# 

class FlashSave

@@default_player = 'totem'
@@mem_id = nil
@@files = []
@@path = Dir.home # You can change the path to anyplace you want
@@file_name = "#{rand(6)}.flv"
@@list_item = []


  def useage()
    puts 'Useage: script.rb /path/to/save/the/file.flv'
    exit
  end


  def save_vid_file(vid_id)

    puts "[+] saved in #{@@path+"/"+@@file_name}" if vid_id !=nil
    system("cp /proc/#{@@mem_id}/fd/#{vid_id} #{@@path}/#{@@file_name}")
   
  end

  def play_selected(vid_id)
    system("#{@@default_player} /proc/#{@@mem_id}/fd/#{vid_id}")
  end

  def list_vid_file()
      @@files.each do |vid|
      puts vid
   end
  end

  def help()
    puts "help          : gets the list of the commands"
    puts "list          : list the number of the vid file"
    puts "play <vid id> : will play the selected file with the given player"
    puts "save          : will save the file "
    puts "quit          : exit the prompt"
  end

  def file_validation(vid_id)
    if @@files.include?(vid_id.to_s) 
          return true
    else 
          puts "[!] Wrong number #{vid_id}, File Doesnt Exsist" 
    end
  end


  def command(in_cmd)
    cmd , pass = in_cmd.split(" ")
    if cmd == 'help'
      help()
    elsif cmd == 'list'
      list_vid_file
    elsif cmd == 'play'
        play_selected(pass) if file_validation(pass)
    elsif cmd == 'save'
      save_vid_file(pass) if file_validation(pass)
    end
  end


  def execute(apass)


    @@path = File.dirname(apass[0]) if apass[0]# You can change the path to anyplace you want
    @@file_name = File.basename(apass[0]) if apass[0]

    flash_id = IO.popen(["pgrep","-f", "flashplayer"]).read
    @@mem_id= flash_id.to_s.chomp
    list = IO.popen(["ls","-l", "/proc/#{@@mem_id}/fd/"]).read
    list_array = list.split("\n")  
    selected = []
    selected << list_array.find_all { |line| /deleted/ =~ line}

    if selected.flatten.empty?
      puts "[!] No flash file has been detected , Exit.."
      exit
    end

    selected.each do |list_item|
      @@list_item = list_item
       list_item.each do |l|     
          p =  l.split(" ")
          @@files <<  p[8]
       end
    end
    if @@list_item.length > 1
     puts "[!] You have more than one flash player loaded in your browser"
      while line = Readline.readline('> ', true)
        command(line)
        break if line == 'quit'
      end
    else
      save_vid_file(@@files[0])
    end

  end

end


save = FlashSave.new()
save.execute(ARGV)










